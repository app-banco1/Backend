import { IsEmail, IsNotEmpty, IsString } from 'class-validator';

export class AuthDTO{
  @IsString()
  @IsNotEmpty()
  password: string;
  
  @IsEmail()
  @IsNotEmpty()
  correo: string;
}