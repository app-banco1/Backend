import { Injectable, UnauthorizedException } from "@nestjs/common";
import { PassportStrategy } from "@nestjs/passport";
import { InjectRepository } from "@nestjs/typeorm";
import { ExtractJwt, Strategy } from "passport-jwt";
import { Repository } from "typeorm";
import { Usuario } from "src/entities/usuario.entity";
import { AuthInterface } from "../interfaces/auth.interface";

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy){
  constructor(
    @InjectRepository(Usuario)
    private usuarioRepository: Repository<Usuario>
  ){
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme('Token'),
      secretOrKey: process.env.SECRET_KEY_JWT
    })
  }

  async validate(payload: AuthInterface){
    const { correo } = payload;
    const user = await this.usuarioRepository.findOne({
      where: {
        correo
      }
    });
    if(!user){
      throw new UnauthorizedException('Credenciales invalidas');
    }
    return user;
  }
}