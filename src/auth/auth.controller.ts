import { Body, Controller, HttpStatus, Post, Res, ValidationPipe, Headers, UnauthorizedException, UseGuards } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthDTO } from './dtos/auth.dto';

@Controller()
export class AuthController {
  constructor(
    private authService: AuthService
  ){}

  @Post('login')
  async login(@Res() res, @Body(ValidationPipe) credenciales: AuthDTO){
    const token = await this.authService.login(credenciales);
    return res.status(HttpStatus.OK).json({
      statusCode: 200,
      message: 'logeado',
      token: token
    });
  }
}
