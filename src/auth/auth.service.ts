import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import { Usuario } from 'src/entities/usuario.entity';
import { Repository } from 'typeorm';
import { AuthDTO } from './dtos/auth.dto';

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(Usuario)
    private readonly usuarioRepository: Repository<Usuario>,
    private jwtService: JwtService
  ){}

  async login(credenciales: AuthDTO){
    try {
      const user = await this.usuarioRepository.findOne({
        select: {
          idUsuario: true,
          correo: true,
          idCuenta: true,
          password: true,
          username: true,
          idCuenta2:{
            montoDisponible: true
          }
        },
        where:{
          'correo': credenciales.correo
        },
        relations:{
          idCuenta2: true
        }
      });
      const isValid = await user.comparePassword(credenciales.password);
      if(!isValid){
          throw new UnauthorizedException('Credenciales invalidas');
      }
      const payload = {correo: user.correo, idUsuario: user.idUsuario, idCuenta: user.idCuenta, montoDisponible: user.idCuenta2.montoDisponible, nombre: user.username};
      const token = this.jwtService.sign(payload);
      return token;
    } catch (error) {
      throw new UnauthorizedException('Credenciales invalidas');
    }
  }
}
