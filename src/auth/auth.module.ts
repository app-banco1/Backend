import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Cuenta } from 'src/entities/cuenta.entity';
import { Movimiento } from 'src/entities/movimiento.entity';
import { Usuario } from 'src/entities/usuario.entity';
import { UsuarioService } from 'src/usuario/usuario.service';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { JwtStrategy } from './strategys/jwt.strategy';

@Module({
  imports:[
    TypeOrmModule.forFeature([
      Usuario,
      Cuenta,
      Movimiento
    ]),
    JwtModule.register({
      secret: process.env.SECRET_KEY_JWT,
      signOptions:{
        expiresIn: '1d'
      }
    }),
    PassportModule.register({
      defaultStrategy: 'jwt'
    })
  ],
  controllers: [
    AuthController
  ],
  providers: [
    AuthService,
    JwtStrategy,
    UsuarioService
  ],
  exports: [
    PassportModule,
    JwtStrategy,
    AuthService
  ]
})
export class AuthModule {}
