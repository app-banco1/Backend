import { Module } from '@nestjs/common';
import { AuthModule } from './auth/auth.module';
import { UsuarioModule } from './usuario/usuario.module';
import { DestinatarioModule } from './destinatario/destinatario.module';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: process.env.NODE_ENV == 'develop' ? 'mariadb' : 'mysql',
      host: process.env.DATABASE_HOST,
      //socketPath: process.env.SOCKET_PATH,
      port: Number(process.env.DATABASE_PORT),
      username: process.env.DATABASE_USER,
      password: process.env.DATABASE_PASSWORD,
      database: process.env.DATABASE_DB,
      entities: [__dirname + '/**/*.entity{.ts,.js}'],
      synchronize: true,
    }),
    AuthModule, 
    UsuarioModule, 
    DestinatarioModule],
  controllers: [],
  providers: [],
})
export class AppModule {}
