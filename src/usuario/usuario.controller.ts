import { Body, Controller, Get, HttpStatus, NotFoundException, Post, Res, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AuthService } from 'src/auth/auth.service';
import { User } from 'src/comun/user.decorator';
import { Usuario } from 'src/entities/usuario.entity';
import { CrearTransferenciaDTO, CrearUsuarioDTO } from './dtos/usuario.dto';
import { UsuarioService } from './usuario.service';

@Controller('usuario')
export class UsuarioController {
  constructor(
    private usuarioService: UsuarioService,
    private authService: AuthService
  ){}

  @Post('crear')
  async registro(@Res() res, @Body() usuarioDTO: CrearUsuarioDTO){
    const usuario = await this.usuarioService.create(usuarioDTO);
    if (!usuario){
        throw new NotFoundException('No se pudo ingresar la informacion.');
    }
    const token = await this.authService.login({password: usuarioDTO.password, correo: usuarioDTO.correo});
    return res.status(HttpStatus.OK).json({
        statusCode: 200,
        message: 'creado',
        token
    });
  }

  @Post('transferir')
  @UseGuards(AuthGuard())
  async transferir(@Res() res, @User() usuario: Usuario, @Body() transferenciaDTO: CrearTransferenciaDTO){
    const user = usuario.toJSON();
    const transferencia = await this.usuarioService.transfer(user.idUsuario, transferenciaDTO);
    if (!transferencia){
        throw new NotFoundException('No se pudo realizar la transferencia.');
    }
    return res.status(HttpStatus.OK).json({
        statusCode: 200,
        message: 'transferido',
        object: transferencia
    });
  }

  @Get('cuenta')
  @UseGuards(AuthGuard())
  async verCuenta(@Res() res, @User() usuario: Usuario){
    const user = usuario.toJSON()
    const {idCuenta,montoDisponible} = await this.usuarioService.cuentaUsuario(user.idUsuario);
    if (!montoDisponible || !idCuenta){
        throw new NotFoundException('No se encuentra informacion.');
    }
    return res.status(HttpStatus.OK).json({
        numero: idCuenta,
        monto: montoDisponible
    });
  }

  @Get('movimientos')
  @UseGuards(AuthGuard())
  async verMovimientos(@Res() res, @User() usuario: Usuario){
    const user = usuario.toJSON()
    const movimientos = await this.usuarioService.findAllMovimientos(user.idCuenta);
    if (!movimientos || movimientos.length == 0){
        throw new NotFoundException('No se encuentra informacion.');
    }
    return res.status(HttpStatus.OK).json({
        statusCode: 200,
        message: 'encontrados',
        object: movimientos
    });
  }
}
