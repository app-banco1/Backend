import { IsEmail, IsNotEmpty, IsNumber, IsString, MaxLength } from "class-validator";

export class CrearUsuarioDTO{
  @IsString()
  @IsNotEmpty()
  @MaxLength(45)
  username: string;

  @IsEmail()
  @IsNotEmpty()
  @MaxLength(150)
  correo: string;

  @IsString()
  @IsNotEmpty()
  password: string;
  
  idCuenta?: number;
}

export class CrearTransferenciaDTO{
  @IsNumber()
  @IsNotEmpty()
  idDestinatario: number;

  @IsNumber()
  @IsNotEmpty()
  monto: number;

  idCuenta?: number;

  idDestinatario2?:{}
}