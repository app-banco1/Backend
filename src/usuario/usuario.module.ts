import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from 'src/auth/auth.module';
import { AuthService } from 'src/auth/auth.service';
import { Cuenta } from 'src/entities/cuenta.entity';
import { Movimiento } from 'src/entities/movimiento.entity';
import { Usuario } from 'src/entities/usuario.entity';
import { UsuarioController } from './usuario.controller';
import { UsuarioService } from './usuario.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Usuario, 
      Cuenta,
      Movimiento
    ]),
    AuthModule
  ],
  controllers: [
    UsuarioController
  ],
  providers: [
    UsuarioService
  ],
  exports: [
    UsuarioService
  ]
})
export class UsuarioModule {}
