import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Cuenta } from 'src/entities/cuenta.entity';
import { Movimiento } from 'src/entities/movimiento.entity';
import { Usuario } from 'src/entities/usuario.entity';
import { Repository } from 'typeorm';
import { CrearTransferenciaDTO, CrearUsuarioDTO } from './dtos/usuario.dto';

@Injectable()
export class UsuarioService {
  constructor(
    @InjectRepository(Usuario)
    private readonly usuarioRepository: Repository<Usuario>,
    @InjectRepository(Cuenta)
    private readonly cuentaRepository: Repository<Cuenta>,
    @InjectRepository(Movimiento)
    private readonly movimientosRepository: Repository<Movimiento>,
  ){}

  async create(usuarioDTO: CrearUsuarioDTO){
    try {
      const cuenta = this.cuentaRepository.create();
      await cuenta.save();
      usuarioDTO.idCuenta = cuenta.idCuenta;

      const usuario = this.usuarioRepository.create(usuarioDTO);
      await usuario.save();
      return {
        idCuenta: cuenta.idCuenta, 
        idUsuario: usuario.idUsuario, 
        montoDisponible: cuenta.montoDisponible
      };
    } catch (error) {
      throw new InternalServerErrorException();
    }
  }

  async transfer(idUsuario: number, crearTransferenciaDTO: CrearTransferenciaDTO){
    try {
      delete crearTransferenciaDTO.idDestinatario2;
      /* Trae informacion de la cuenta del usuario que realiza la transaccion */
      const {idCuenta, montoDisponible} = await this.cuentaUsuario(idUsuario);

      /* Revisa si se puede realizar la transaccion */
      if(montoDisponible < crearTransferenciaDTO.monto){
        throw new InternalServerErrorException('Monto de transaccion supera el permitido');
      }

      /* Desuenta el monto de la cuenta del usuario */
      let montoNuevo = montoDisponible - crearTransferenciaDTO.monto;
      await this.cuentaRepository
        .createQueryBuilder()
        .update(Cuenta)
        .set({ montoDisponible: montoNuevo})
        .where("id_cuenta = :id", { id: idCuenta })
        .execute()
      
      crearTransferenciaDTO.idCuenta = idCuenta;
      /* Guarda registro del movimiento */
      const transferencia = this.movimientosRepository.create(crearTransferenciaDTO);
      await transferencia.save()
      return transferencia;
    } catch (error) {
      throw error;
    }
  }

  async cuentaUsuario(idUsuario: number){
    try {
      const objeto = await this.usuarioRepository.findOne({
          where: {
            idUsuario
          },
          relations: {
            idCuenta2: true
          }
      });
      return objeto.idCuenta2;
    } catch (error) {
      throw new InternalServerErrorException();
    }
  }

  async findUsuario(idUsuario: number){
    try {
      const objeto = await this.usuarioRepository.findOne({
          where: {
            idUsuario
          },
          relations: {
            idCuenta2: true
          }
      });
      return {
        idCuenta: objeto.idCuenta, 
        idUsuario: objeto.idUsuario, 
        montoDisponible: objeto.idCuenta2.montoDisponible
      };
    } catch (error) {
      throw new InternalServerErrorException();
    }
  }

  async findAllMovimientos(idCuenta: number){
    try {
      const objeto = await this.movimientosRepository.find({
          select:{
            idMovimiento: true,
            idDestinatario: true,
            monto: true,
            idDestinatario2: {
              nombre: true
            }
          },
          where: {
            idCuenta
          },
          relations:{
            idDestinatario2: true
          }
      });
      return objeto;
    } catch (error) {
      throw new InternalServerErrorException();
    }
  }
}
