import { BaseEntity, BeforeInsert, Column, Entity, Index, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import * as bcrypt from 'bcryptjs';
import { Destinatario } from "./destinatario.entity";
import { Cuenta } from "./cuenta.entity";
import { Exclude, instanceToPlain } from "class-transformer";
  
@Index("fk_cuenta_idx", ["idCuenta"], {})
@Entity("TBL_USUARIO", { schema: "mydb" })
export class Usuario extends BaseEntity{
  @PrimaryGeneratedColumn({ type: "int", name: "id_usuario" })
  idUsuario: number;
  
  @Column("int", { name: "id_cuenta"})
  idCuenta: number;

  @Column("varchar", { name: "username", length: 45 })
  username: string;

  @Column("varchar", { name: "correo", length: 150 })
  correo: string;
  
  @Column("varchar", { name: "password", length: 250 })
  @Exclude()
  password: string;

  @BeforeInsert()
  async hashPassword(){
    this.password = await bcrypt.hash(this.password, 10);
  }

  async comparePassword(password: string){
    return await bcrypt.compare(password, this.password);
  }
  
  toJSON(){
    return instanceToPlain(this);
  }

  @OneToMany(
    () => Destinatario,
    (destinatario) => destinatario.idUsuario2
  )
  destinatarios: Destinatario[];

  @ManyToOne(() => Cuenta, (cuenta) => cuenta.usuarios, {
    onDelete: "NO ACTION",
    onUpdate: "NO ACTION",
  })
  @JoinColumn([{ name: "id_cuenta", referencedColumnName: "idCuenta" }])
  idCuenta2: Cuenta;
}
  