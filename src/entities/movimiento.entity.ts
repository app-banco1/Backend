import { BaseEntity, Column, Entity, Index, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Destinatario } from "./destinatario.entity";
import { Cuenta } from "./cuenta.entity";
  
@Index("fk_cuenta_idx", ["idCuenta"], {})
@Index("fk_destinatario_idx", ["idDestinatario"], {})
@Entity("TBL_MOVIMIENTO", { schema: "mydb" })
export class Movimiento extends BaseEntity{
  @PrimaryGeneratedColumn({ type: "int", name: "id_movimiento" })
  idMovimiento: number;

  @Column("int", { name: "id_destinatario"})
  idDestinatario: number;

  @Column("int", { name: "id_cuenta"})
  idCuenta: number;

  @Column("int", { name: "monto"})
  monto: number;

  @ManyToOne(
    () => Destinatario,
    (destinatario) => destinatario.movimientos,
    { onDelete: "NO ACTION", onUpdate: "NO ACTION" }
  )
  @JoinColumn([{ name: "id_destinatario", referencedColumnName: "idDestinatario" }])
  idDestinatario2: Destinatario;

  @ManyToOne(() => Cuenta, (cuenta) => cuenta.movimientos, {
    onDelete: "NO ACTION",
    onUpdate: "NO ACTION",
  })
  @JoinColumn([{ name: "id_cuenta", referencedColumnName: "idCuenta" }])
  idCuenta2: Cuenta;
}
  