import { BaseEntity, Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Movimiento } from "./movimiento.entity";
import { Usuario } from "./usuario.entity";

@Entity("TBL_CUENTA", { schema: "mydb" })
export class Cuenta extends BaseEntity {
  @PrimaryGeneratedColumn({ type: "int", name: "id_cuenta" })
  idCuenta: number;

  @Column("int", { name: "monto_disponible", default: 100})
  montoDisponible: number;

  @OneToMany(() => Movimiento, (movimiento) => movimiento.idCuenta2)
  movimientos: Movimiento[];

  @OneToMany(() => Usuario, (usuario) => usuario.idCuenta2)
  usuarios: Usuario[];
}
