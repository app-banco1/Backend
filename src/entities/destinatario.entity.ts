import { BaseEntity, Column, Entity, Index, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Usuario } from "./usuario.entity";
import { Movimiento } from "./movimiento.entity";
  
@Index("fk_usuario_idx", ["idUsuario"], {})
@Entity("TBL_DESTINATARIO", { schema: "mydb" })
export class Destinatario extends BaseEntity{
  @PrimaryGeneratedColumn({ type: "int", name: "id_destinatario" })
  idDestinatario: number;

  @Column("int", { name: "id_usuario", nullable: true})
  idUsuario: number | null;

  @Column("varchar", { name: "nombre", length: 45 })
  nombre: string;

  @Column("varchar", { name: "correo", length: 45 })
  correo: string;

  @Column("int", { name: "cuenta"})
  cuenta: number;
  
  @ManyToOne(() => Usuario, (usuario) => usuario.destinatarios, {
    onDelete: "NO ACTION",
    onUpdate: "NO ACTION",
  })
  @JoinColumn([{ name: "id_usuario", referencedColumnName: "idUsuario" }])
  idUsuario2: Usuario;

  @OneToMany(() => Movimiento, (movimiento) => movimiento.idDestinatario2)
  movimientos: Movimiento[];
}