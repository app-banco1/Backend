require('dotenv').config();
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.enableCors({"origin": "http://localhost:4200"});
  //app.enableCors({"origin": "https://app-banco-4ir6hpkkoa-tl.a.run.app"});

  await app.listen(process.env.PORT || 3000);
}
bootstrap();
