import { Body, Controller, Get, HttpStatus, NotFoundException, Param, Post, Res, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { User } from 'src/comun/user.decorator';
import { Usuario } from 'src/entities/usuario.entity';
import { DestinatarioService } from './destinatario.service';
import { CrearDestinatarioDTO } from './dtos/destinatario.dto';

@Controller('destinatario')
export class DestinatarioController {
  constructor(
    private destinatarioService: DestinatarioService
  ){}

  @Post('crear')
  @UseGuards(AuthGuard())
  async crear(@Res() res, @User() usuario: Usuario, @Body() destinatarioDTO: CrearDestinatarioDTO){
    const user = usuario.toJSON();
    const destinatario = await this.destinatarioService.create(user.idUsuario, destinatarioDTO);
    if (!destinatario){
        throw new NotFoundException('No se pudo ingresar la informacion.');
    }
    return res.status(HttpStatus.OK).json({
        statusCode: 200,
        message: 'creado',
        object: destinatario
    });
  }

  @Get('todos')
  @UseGuards(AuthGuard())
  async buscarTodos(@Res() res, @User() usuario: Usuario){
    const user = usuario.toJSON()
    const destinatarios = await this.destinatarioService.findAll(user.idUsuario);
    if (!destinatarios || destinatarios.length == 0){
        throw new NotFoundException('No se encuentra informacion.');
    }
    return res.status(HttpStatus.OK).json({
        statusCode: 200,
        message: 'encontrados',
        object: destinatarios
    });
  }

  @Get(':id')
  @UseGuards(AuthGuard())
  async buscar(@Res() res, @User() usuario: Usuario, @Param('id') idDestinatario){
    const destinatario = await this.destinatarioService.find(idDestinatario);
    if (!destinatario){
        throw new NotFoundException('No se encuentra informacion.');
    }
    return res.status(HttpStatus.OK).json({
        statusCode: 200,
        message: 'encontrado',
        object: destinatario
    });
  }
}
