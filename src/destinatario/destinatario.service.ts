import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Destinatario } from 'src/entities/destinatario.entity';
import { Repository } from 'typeorm';
import { CrearDestinatarioDTO } from './dtos/destinatario.dto';

@Injectable()
export class DestinatarioService {
  constructor(
    @InjectRepository(Destinatario)
    private readonly destinatarioRepository: Repository<Destinatario>
  ){}

  async create(idUsuario: number, destinatarioDTO: CrearDestinatarioDTO){
    try {
      destinatarioDTO.idUsuario = idUsuario;
      const destinatario = this.destinatarioRepository.create(destinatarioDTO);
      await destinatario.save();
      return destinatario;
    } catch (error) {
      throw new InternalServerErrorException();
    }
  }

  async findAll(idUsuario: number){
    try {
      const destinatarios = await this.destinatarioRepository.find({
        where: {
          idUsuario
        }
      });
      return destinatarios;
    } catch (error) {
        throw new InternalServerErrorException();
    }
  }

  async find(idDestinatario: number){
    try {
      const destinatario = await this.destinatarioRepository.find({
        where: {
          idDestinatario
        }
      });
      return destinatario;
    } catch (error) {
        throw new InternalServerErrorException();
    }
  }
}
