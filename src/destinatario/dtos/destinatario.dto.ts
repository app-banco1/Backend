import { IsEmail, IsNotEmpty, IsNumber, IsString, MaxLength } from "class-validator";

export class CrearDestinatarioDTO{
  @IsString()
  @IsNotEmpty()
  @MaxLength(45)
  nombre: string;

  @IsEmail()
  @IsNotEmpty()
  @MaxLength(150)
  correo: string;

  @IsNumber()
  @IsNotEmpty()
  cuenta: number;
  
  idUsuario?: number;
}