import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from 'src/auth/auth.module';
import { Destinatario } from 'src/entities/destinatario.entity';
import { DestinatarioController } from './destinatario.controller';
import { DestinatarioService } from './destinatario.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Destinatario
    ]),
    AuthModule
  ],
  controllers: [
    DestinatarioController
  ],
  providers: [
    DestinatarioService
  ]
})
export class DestinatarioModule {}
