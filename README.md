## Descipcion
Backend de app desafio tecnico.

## Instalacion
1. Instalar Nest CLI
```bash
$ npm install -g @nestjs/cli
```
2. Realizar instalacion de plugins
```bash
$ npm install
```
3. Levantar la base de datos
```bash
$ docker-compose up -d
```

## Iniciar aplicacion
Para iniciar el modo develop.

```bash
$ npm run start:dev
```