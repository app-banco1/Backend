FROM node:16-alpine3.15
RUN mkdir -p /app
WORKDIR /app
COPY package*.json /app/
COPY .env /app/
RUN npm install
COPY ./dist /app/dist
# RUN npm run build --prod

CMD [ "node", "dist/main.js" ]