git:
	git pull
	git add .
	git commit -m "$m"
	git push

docker:
	npm run build
	docker build . -t southamerica-west1-docker.pkg.dev/app-banco-358223/app-banco/backend:latest -t southamerica-west1-docker.pkg.dev/app-banco-358223/app-banco/backend:$v
	docker push southamerica-west1-docker.pkg.dev/app-banco-358223/app-banco/backend:$v
	docker push southamerica-west1-docker.pkg.dev/app-banco-358223/app-banco/backend:latest